<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>POST Update Profile</name>
   <tag></tag>
   <elementGuidId>d0ec42d2-e225-48f3-9894-df34b7a3d477</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>${GlobalVariable.token}</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;${name}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;${whatsapp}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;${birth_date}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;${bio}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;${position}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>ecfe067b-2ee5-4951-880a-bdb184d06f93</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.token}</value>
      <webElementGuid>bcf98150-8a18-49b5-a2c6-5d8aa68728c0</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.6</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.baseUrl}updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'Arif Iqbal'</defaultValue>
      <description></description>
      <id>bec9878a-4754-4b48-8fbe-4c94abf02b50</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>'085158851926'</defaultValue>
      <description></description>
      <id>f4d914d7-964f-4df7-bede-560be88d0d82</id>
      <masked>false</masked>
      <name>whatsapp</name>
   </variables>
   <variables>
      <defaultValue>'1996-06-10'</defaultValue>
      <description></description>
      <id>d0090e22-119c-4cdc-823f-d91a720c52ce</id>
      <masked>false</masked>
      <name>birth_date</name>
   </variables>
   <variables>
      <defaultValue>'cimiw simiw'</defaultValue>
      <description></description>
      <id>43037311-0916-4021-b8a8-dd89f016249f</id>
      <masked>false</masked>
      <name>bio</name>
   </variables>
   <variables>
      <defaultValue>'web dev'</defaultValue>
      <description></description>
      <id>00fb739b-f542-4ae1-823d-35ff1e7e4ba7</id>
      <masked>false</masked>
      <name>position</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

KeywordUtil.logInfo(request.getRequestAuthorization())

//assertThat(response.getStatusCode()).isEqualTo(200)

//assertThat(response.getResponseText()).contains('success')

KeywordUtil.logInfo(GlobalVariable.token)

if(GlobalVariable.token == null) {
	assertThat(response.getStatusCode()).isEqualTo(302)
} else {
	assertThat(response.getStatusCode()).isEqualTo(200)
}

if(response.getResponseText().contains('success')) {
	assertThat(response.getStatusCode()).isEqualTo(200)
} else if (response.getResponseText().contains('failed')) {
	assertThat(response.getStatusCode()).isEqualTo(403)
}</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
